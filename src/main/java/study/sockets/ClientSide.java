package study.sockets;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientSide {

    private final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws UnknownHostException {
        new ClientSide().start();
    }

    public void start() throws UnknownHostException {
        InetAddress addr = InetAddress.getByName("localhost");

        int port = askPort();
        String name = askName();
        try (Socket socket = new Socket(addr, port)) {
            System.out.println("Connected!");

            SocketConsoleProcessor processor = new ClientSocketConsoleProcessor(socket, name);
            processor.process();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String askName() {
        System.out.print("Enter your name: ");
        String s = scanner.next();
        scanner.nextLine();
        return s;
    }

    private int askPort() {
        System.out.print("Enter the port: ");
        int i = scanner.nextInt();
        scanner.nextLine();
        return i;
    }
}
