package study.sockets;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;

public class ClientSocketConsoleProcessor extends SocketConsoleProcessor {
    private String name;

    public ClientSocketConsoleProcessor(Socket socket, String name) {
        super(socket);
        this.name = name;
    }

    @Override
    public void process() {
        super.process();
    }

    @Override
    protected SocketSender initSocketSender(PrintStream socketOut) {
        return new ClientSocketSender(socketOut);
    }

    @Override
    protected SocketReceiver initSocketReceiver(InputStream socketIn) {
        return new ClientSocketReceiver(socketIn);
    }

    protected class ClientSocketSender extends SocketConsoleProcessor.SocketSender {
        public ClientSocketSender(PrintStream socketOut) {
            super(socketOut);
            socketOut.println(name + " connected!");
        }

        @Override
        protected void printToSocket(String s) {
            socketOut.printf("[%s]: %s", name, s);
            socketOut.println();
        }
    }


    private class ClientSocketReceiver extends SocketConsoleProcessor.SocketReceiver {
        public ClientSocketReceiver(InputStream socketIn) {
            super(socketIn);
        }

        @Override
        protected boolean getExitCondition(String s) {
            return s.equals("SERVER: exit");
        }
    }
}
