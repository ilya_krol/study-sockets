package study.sockets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSide {

    public static void main(String[] args) {
        new ServerSide().start();
    }

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(0)) {

            System.out.println("Port: " + serverSocket.getLocalPort());

            System.out.println("Waiting...");

            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Connected!");

                Thread thread = new Thread(() -> {
                    System.out.println("thread started");
                    SocketConsoleProcessor processor = new ServerSocketConsoleProcessor(socket);
                    processor.process();
                    System.out.println("thread ended");
                });
                thread.start();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
