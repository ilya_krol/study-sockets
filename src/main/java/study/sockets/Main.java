package study.sockets;

import java.net.UnknownHostException;

public class Main {
    public static void main(String[] args) throws UnknownHostException {
        if (args.length > 0) {
            String arg = args[0];

            switch (arg) {
                case "server":
                    startServer();
                    break;

                case "client":
                    startClient();
                    break;
            }
        }
    }

    private static void startClient() throws UnknownHostException {
        ClientSide clientSide = new ClientSide();
        clientSide.start();
    }

    private static void startServer() {
        ServerSide serverSide = new ServerSide();
        serverSide.start();
    }
}
