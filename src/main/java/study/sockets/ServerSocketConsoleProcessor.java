package study.sockets;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;

public class ServerSocketConsoleProcessor extends SocketConsoleProcessor {
    public ServerSocketConsoleProcessor(Socket socket) {
        super(socket);
    }

    @Override
    protected SocketSender initSocketSender(PrintStream socketOut) {
        return new ServerSocketSender(socketOut);
    }

    @Override
    protected SocketReceiver initSocketReceiver(InputStream socketIn) {
        return new ServerSocketReceiver(socketIn);
    }

    private class ServerSocketSender extends SocketSender {
        ServerSocketSender(PrintStream socketOut) {
            super(socketOut);
        }

        @Override
        protected void printToSocket(String s) {
            socketOut.printf("SERVER: %s", s);
            socketOut.println();
        }
    }


    private class ServerSocketReceiver extends SocketReceiver {
        ServerSocketReceiver(InputStream socketIn) {
            super(socketIn);
        }

        @Override
        protected boolean getExitCondition(String s) {
            return s.endsWith("exit");
        }
    }

}
