package study.sockets;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public abstract class SocketConsoleProcessor {

    private Socket socket;
    private InputStream socketIn;
    protected PrintStream socketOut;

    private boolean halt = false;

    public SocketConsoleProcessor(Socket socket) {
        this.socket = socket;
    }

    public void process() {
        if (!initStreams())
            return;

        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(initSocketReceiver(socketIn));
        executorService.execute(initSocketSender(socketOut));

        try {
            waiting();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        executorService.shutdownNow();
    }

    protected abstract SocketReceiver initSocketReceiver(InputStream socketIn);

    protected abstract SocketSender initSocketSender(PrintStream socketOut);

    private synchronized void waiting() throws InterruptedException {
        while (!halt)
            wait();
    }

    private synchronized void halt() {
        halt = true;
        notifyAll();
    }


    private boolean initStreams() {
        try {
            socketIn = socket.getInputStream();
            socketOut = new PrintStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    protected abstract class SocketReceiver implements Runnable {
        private InputStream socketIn;

        SocketReceiver(InputStream socketIn) {
            this.socketIn = socketIn;
        }

        @Override
        public void run() {
            Scanner scanner = new Scanner(socketIn);

            while (true) {
                if (!waitStream(socketIn))
                    return;

                String s = scanner.nextLine();
                System.out.println(s);

                if (getExitCondition(s)) {
                    System.out.println("Exiting...");
                    halt();
                    break;
                }
            }
        }

        protected abstract boolean getExitCondition(String s);

    }

    protected abstract class SocketSender implements Runnable {
        protected PrintStream socketOut;

        SocketSender(PrintStream socketOut) {
            this.socketOut = socketOut;
        }

        @Override
        public void run() {
            Scanner scanner = new Scanner(System.in);

            while (true) {
                if (!waitStream(System.in))
                    return;

                String s = scanner.nextLine();
                printToSocket(s);

                if (s.equals("exit")) {
                    System.out.println("Exiting...");
                    halt();
                    break;
                }
            }
        }

        protected abstract void printToSocket(String s);
    }

    private boolean waitStream(InputStream inputStream) {
        try {
            while (!halt && !Thread.currentThread().isInterrupted() && inputStream.available() == 0)
                MILLISECONDS.sleep(100);
        } catch (InterruptedException | IOException e) {
            return false;
        }
        return true;
    }
}
